import React from 'react';
import axios from  'axios';

export default class Advertisement extends React.Component {

  constructor()
  {
      super();
      this.state={
          apidata:[]
      }
  }

  componentDidMount()
  {
      axios.get('https://jsonplaceholder.typicode.com/posts')
      .then(response=>{
          this.setState({apidata:response.data});
      });
  }

    
  render() {
    return (
        <div>
            <table className="table">
                <tbody>
                    {
                        this.state.apidata.map(apidata=>{
                        return(
                        <tr>  
                        <th scope="row">{apidata.id}</th>
                        <tr>
                        <th scope="row">{apidata.title}</th>
                        </tr>
                        </tr>  
                        );
                        })
                    }
                </tbody>
            </table>
        </div>
    )
  }
}