// @material-ui/icons
import Dashboard from "@material-ui/icons/Dashboard";
import Person from "@material-ui/icons/Person";
import LibraryBooks from "@material-ui/icons/LibraryBooks";
import BubbleChart from "@material-ui/icons/BubbleChart";
import LocationOn from "@material-ui/icons/LocationOn";
import Notifications from "@material-ui/icons/Notifications";
import Unarchive from "@material-ui/icons/Unarchive";
// core components/views for Admin layout
import DashboardPage from "views/Dashboard/Dashboard.js";
import Analytics from "views/Analytics/Analytics.js";
import Event from "views/Event/Event.js";
import Message from "views/Message/Message.js";
import SNS from "views/SNS/SNS.js";
import elearning from "views/elearning/elearning.js";
import Advertisement from "views/Advertisement/Advertisement.js";
import Jobinfo from "views/Jobinfo/Jobinfo.js";
import Profile from "views/Profile/Profile.js";
import Content from "views/Content/Content.js";

const dashboardRoutes = [
  {
    path: "/dashboard",
    name: "Dashboard",
    icon: Dashboard,
    component: DashboardPage,
    layout: "/admin"
  },
  {
    path: "/analytics",
    name: "Analytics",
    icon: Person,
    component: Analytics,
    layout: "/admin"
  },
  {
    path: "/event",
    name: "Event",
    icon: "content_paste",
    component: Event,
    layout: "/admin"
  },
  {
    path: "/message",
    name: "Message",
    icon: LibraryBooks,
    component: Message,
    layout: "/admin"
  },
  {
    path: "/sns",
    name: "SNS",
    icon: BubbleChart,
    component: SNS,
    layout: "/admin"
  },
  {
    path: "/elearning",
    name: "E-learning",
    icon: LocationOn,
    component: elearning,
    layout: "/admin"
  },
  {
    path: "/ad",
    name: "Advertisement",
    icon: Notifications,
    component: Advertisement,
    layout: "/admin"
  },
  {
    path: "/jobinfo",
    name: "Jobinfo",
    icon: Unarchive,
    component: Jobinfo,
    layout: "/admin"
  },
  {
    path: "/profile",
    name: "Profile",
    icon: Unarchive,
    component: Profile,
    layout: "/admin"
  },
  {
    path: "/content",
    name: "Content",
    icon: Unarchive,
    component: Content,
    layout: "/admin"
  }
];

export default dashboardRoutes;
