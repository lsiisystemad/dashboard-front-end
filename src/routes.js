// @material-ui/icons
import Dashboard from "@material-ui/icons/Dashboard";
import AssessmentIcon from '@material-ui/icons/Assessment';
import EventNoteIcon from '@material-ui/icons/EventNote';
import MessageIcon from '@material-ui/icons/Message';
import PeopleIcon from '@material-ui/icons/People';
import CastForEducationIcon from '@material-ui/icons/CastForEducation';
import Notifications from "@material-ui/icons/Notifications";
import WorkOutlineIcon from '@material-ui/icons/WorkOutline';
import PersonPinIcon from '@material-ui/icons/PersonPin';
import Unarchive from "@material-ui/icons/Unarchive";
// core components/views for Admin layout
import DashboardPage from "views/Dashboard/Dashboard.js";
import Analytics from "views/Analytics/Analytics.js";
import Events from "views/Events/Events.js";
import Messages from "views/Messages/Messages.js";
import SNS from "views/SNS/SNS.js";
import Elearning from "views/Elearning/Elearning.js";
import Advertisements from "views/Advertisements/Advertisements.js";
import Jobinfo from "views/Jobinfo/Jobinfo.js";
import UserProfile from "views/UserProfile/UserProfile.js";
import Contents from "views/Contents/Contents.js";
import Dummy from "views/Dummy/Dummy";

const dashboardRoutes = [
  {
    path: "/dashboard",
    name: "Dashboard",
    icon: Dashboard,
    component: DashboardPage,
    layout: "/admin"
  },
  {
    path: "/analytics",
    name: "Analytics",
    icon: AssessmentIcon,
    component: Analytics,
    layout: "/admin"
  },
  {
    path: "/events",
    name: "Events",
    icon: EventNoteIcon,
    component: Events,
    layout: "/admin"
  },
  {
    path: "/messages",
    name: "Messages",
    icon: MessageIcon,
    component: Messages,
    layout: "/admin"
  },
  {
    path: "/sns",
    name: "SNS",
    icon: PeopleIcon,
    component: SNS,
    layout: "/admin"
  },
  {
    path: "/elearning",
    name: "E-Learning",
    icon: CastForEducationIcon,
    component: Elearning,
    layout: "/admin"
  },
  {
    path: "/advertisements",
    name: "Advertisements",
    icon: Notifications,
    component: Advertisements,
    layout: "/admin"
  },
  {
    path: "/jobinfo",
    name: "Jobinfo",
    icon: WorkOutlineIcon,
    component: Jobinfo,
    layout: "/admin"
  },
  {
    path: "/profile",
    name: "User Profile",
    icon: PersonPinIcon,
    component: UserProfile,
    layout: "/admin"
  },
  {
    path: "/contents",
    name: "Contents",
    icon: Unarchive,
    component: Contents,
    layout: "/admin"
  },
  {
    path: "/dummy",
    name: "Dummy",
    icon: Unarchive,
    component: Dummy,
    layout: "/admin"
  }
 ];

export default dashboardRoutes;
